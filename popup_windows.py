#!/usr/bin/python
#
# $Id: crose 2015-02-16 17:13:14+01:00$
#
#

import Tkinter

# ------------------- Popup window to ask whether changes should be saved at quitting --------------------
class popup_mute(Tkinter.Tk):
	def __init__(self,parent):
		Tkinter.Tk.__init__(self,parent)
		self.parent = parent
		
		self.initialize()
	
	
	def initialize(self):
		self.grid()
		self.minsize(width=350,height=111)
		label = Tkinter.Label(self,text='No Sound is recorded at the moment. \nTo record again, close this window.', font='bold')
		label.grid(column=0,row=0, sticky = 'EW')
		buttonCancel = Tkinter.Button(self,text='Close', command=self.cancel)
		buttonCancel.grid(column=0,row=1)
		self.grid_columnconfigure(0,weight=1)
		self.grid_rowconfigure(0,weight=1)
		
	def cancel(self):
		self.destroy()



def open_popup_mute():
	window = popup_mute(None)
	window.title('SSR is mute')
	window.mainloop()
	window.destroy()

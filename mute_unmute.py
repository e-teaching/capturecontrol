#!/usr/bin/python
#
# $Id: crose 2015-02-16 17:13:14+01:00$
#
#

import subprocess as sp


def find_index(process_name):
	audio = sp.Popen(['pacmd', 'list-source-outputs'],stdout=sp.PIPE)
	out, err = audio.communicate()
	out2 = out[:out.find(process_name)]
	out3 = out2[out2.rfind("index")+7:out2.rfind("index")+10]
	try:
		index = int(out3)
	except ValueError:
		index = None
		print 'process not found'
	return index

def mute(process_name):
	index = find_index(process_name)
	sp.Popen(['pacmd', 'set-source-output-mute', str(index),  '1'],stdout=sp.PIPE)
def unmute(process_name):
	index = find_index(process_name)
	sp.Popen(['pacmd', 'set-source-output-mute', str(index),  '0'],stdout=sp.PIPE)

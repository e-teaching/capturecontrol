#!/usr/bin/python
# -*- coding: iso-8859-1 -*-
#
# $Id: crose 2015-02-16 17:13:14+01:00$
#
#

import Tkinter
import tkMessageBox
import tkFileDialog
import Tkinter as tk
import subprocess as sp
import shutil
FFMPEG_BIN = "ffmpeg"
import glob
import json
import os
import time
from os.path import expanduser
home = expanduser("~")
cc_config = home + '/.capturecontrol/'
SSR_config = home + '/.capturecontrol/SSR/'

from mute_unmute import mute, unmute
from available_video import choose_webcam
from GUVCView_settings import write_guvcview_configures
#from popup_windows import popup_mute


class simpleapp_tk(Tkinter.Tk):
	
	def __init__(self,parent):
		Tkinter.Tk.__init__(self,parent)
		self.parent = parent	
		self.load_settings()
		self.initialize()
		
	
	def initialize(self):
		self.running_processes={}
		self.repeater()		
		self.grid()
		self.minsize(width=800,height=700)

		

		self.SSRProfiles()
		self.webcams = choose_webcam()
		

		# Button for Key-Mon Application
		self.buttonVarKeyMon = Tkinter.StringVar()
		buttonKeyMon = Tkinter.Button(self,textvariable=self.buttonVarKeyMon, command=self.KeyMonButtonClick)
		self.buttonVarKeyMon.set(u"Start Key-Mon")
		

		# Field for extras for key mon
		self.entryKeyMonVar = Tkinter.StringVar()
		self.entry = Tkinter.Entry(self,textvariable=self.entryKeyMonVar)
		self.entry.bind("<Return>",self.KeyMonButtonClick)
		if 'key_mon' in self.data: 
			self.entryKeyMonVar.set(self.data['key_mon'])
		else:
			self.entryKeyMonVar.set('')
		
		label_keymon = Tkinter.Label(self,text='KeyMon Parameters')


		# Dropdown for SSR Input Profiles
		
		self.var_input_profile = Tkinter.StringVar()
		if 'ssr_input' in self.data and self.data['ssr_input'] in self.input_profiles:
			self.var_input_profile.set(self.data['ssr_input'])
		else:
			self.var_input_profile.set(self.short_url(self.ccinput_profiles[0]))		
		self.options_input_profile = Tkinter.OptionMenu(self, self.var_input_profile, *self.input_profiles)		
		label_input_profile = Tkinter.Label(self,text='Video Input Profile')


		# Dropdown for SSR Output Profiles
		self.var_output_profile = Tkinter.StringVar()
		if 'ssr_output' in self.data and self.data['ssr_output'] in self.output_profiles:
			self.var_output_profile.set(self.data['ssr_output'])
		else:
			self.var_output_profile.set(self.short_url(self.ccoutput_profiles[0]))		
		self.options_output_profile = Tkinter.OptionMenu(self, self.var_output_profile, *self.output_profiles)
		self.var_output_profile.trace('w', self.compare_suffix_2)		
		
		label_output_profile = Tkinter.Label(self,text='Video Output Profile')



		# Dropdown menu for choosing Webcam
		self.webcams
		self.var_webcams = Tkinter.StringVar()
		if 'webcam' in self.data and self.data['webcam'] in self.webcams:
			self.var_webcams.set(self.data['webcam'])#self.webcams.iterkeys().next())
		else: 	
			self.var_webcams.set(self.webcams.iterkeys().next())
		self.options_webcams = Tkinter.OptionMenu(self, self.var_webcams, *self.webcams)
		label_webcam = Tkinter.Label(self,text='Choose Webcam')


		# Filename for Video File
		self.entryFileNameVar = Tkinter.StringVar()
		self.file_opt_SSR = options_ssr = {}
		options_ssr['defaultextension'] = '.mkv'
		options_ssr['filetypes'] = [ ('Video Files', '.mkv'),('all files', '.*')]
		options_ssr['initialdir'] =  '/home/matha/Desktop/Output/'
		options_ssr['initialfile'] = 'SSR.mkv'
		options_ssr['parent'] = self
		options_ssr['title'] = 'Choose Filename for SSR Video'
		

		if 'filenameSSR' in self.data:
			filenameSSR = self.data['filenameSSR']
			self.entryFileNameVar.set(filenameSSR)
			options_ssr['initialdir'] =  filenameSSR[:filenameSSR.rfind('/')+1]
			options_ssr['initialfile'] = filenameSSR[filenameSSR.rfind('/')+1:]
		else: 	
			self.entryFileNameVar.set("File for Screen recording")

		self.entrySSR = Tkinter.Button(self, textvariable=self.entryFileNameVar, command=self.asksaveasSSR)		
		label_SSR_Filename = Tkinter.Label(self,text='SSR Video File Name')

		#self.entrySSR = Tkinter.Entry(self,textvariable=self.entryFileNameVar)
		

		# Button for SSR
		self.buttonVarSSR = Tkinter.StringVar()
		buttonSSR = Tkinter.Button(self,textvariable=self.buttonVarSSR, command=self.SSRButtonClick)
		self.buttonVarSSR.set(u"Start SSR")

		# Button for MyPaint
		self.buttonVarMyPaint = Tkinter.StringVar()
		buttonMyPaint = Tkinter.Button(self,textvariable=self.buttonVarMyPaint, command=self.MyPaintButtonClick)

		self.buttonVarMyPaint.set(u"Start MyPaint")

		# Button for Webcam
		self.buttonVarmplayer = Tkinter.StringVar()
		buttonmplayer = Tkinter.Button(self,textvariable=self.buttonVarmplayer, command=self.mplayerButtonClick)
		self.buttonVarmplayer.set(u"Start Webcam")



		# Filename for Video File of webcam
		self.file_opt_webcam = options_webcam = {}
		options_webcam['defaultextension'] = '.mkv'
		options_webcam['filetypes'] = [ ('Video Files', '.mkv'),('all files', '.*')]
		options_webcam['initialdir'] =  '/home/matha/Desktop/Output/'
		options_webcam['initialfile'] = 'WebCam.mkv'
		options_webcam['parent'] = self
		options_webcam['title'] = 'Choose Filename for WebCam Video'
		
		self.entryFileNameWebCam = Tkinter.StringVar()
		if 'filenameWebCam' in self.data:
			filenameWebCam = self.data['filenameWebCam']
			self.entryFileNameWebCam.set(filenameWebCam)
			options_webcam['initialdir'] =  filenameWebCam[:filenameWebCam.rfind('/')+1]
			options_webcam['initialfile'] = filenameWebCam[filenameWebCam.rfind('/')+1:]
		else: 	
			self.entryFileNameWebCam.set("File for Webcam recording")

		self.entryWebCam = Tkinter.Button(self, textvariable=self.entryFileNameWebCam, command=self.asksaveasWebCam)		
		label_WebCamFile = Tkinter.Label(self,text='WebCam Video File')



		# Button for Mute SSR
		self.buttonVarMuteSSR = Tkinter.StringVar()
		buttonMuteSSR = Tkinter.Button(self,textvariable=self.buttonVarMuteSSR, command=self.MuteSSRButtonClick)
		self.buttonVarMuteSSR.set(u"Mute SSR")

		# Button for FastStart
		#self.buttonVarFastStart = Tkinter.StringVar()
		#buttonfaststart = Tkinter.Button(self,textvariable=self.buttonVarFastStart, command=self.FastStartButtonClick)
		#buttonfaststart.grid(column=4,row=15)
		#self.buttonVarFastStart.set(u"Direct Recording")

		# Display what happenend
		self.labelVariable = Tkinter.StringVar()
		label = Tkinter.Label(self, textvariable=self.labelVariable, anchor="w",fg="white",bg="blue")
		self.labelVariable.set(u"Hello")


		# Button for Refresh
		self.buttonVarRefresh = Tkinter.StringVar()
		buttonRefresh = Tkinter.Button(self,textvariable=self.buttonVarRefresh, command=self.RefreshButtonClick)	
		self.buttonVarRefresh.set(u"Refresh GUI")

		
		#self.resizable(True, False)

		
		#self.bind('<Control-Key-r>',self.key)

	
		# Filename for choosing file to edit audio
		self.file_opt_audio = options_audio = {}
		options_audio['defaultextension'] = '.mkv'
		options_audio['filetypes'] = [ ('Video Files', '.mkv'),('all files', '.*')]
		options_audio['initialdir'] =  options_ssr['initialdir']
		options_audio['initialfile'] = options_ssr['initialfile']
		options_audio['parent'] = self
		options_audio['title'] = 'Choose Filename to edit Audio'
		
		self.entryFileNameAudio = Tkinter.StringVar()
		self.entryFileNameAudio.set(self.entryFileNameVar.get())
		self.entryAudio = Tkinter.Button(self, textvariable=self.entryFileNameAudio, command=self.askopenAudio)		
		label_audio_file = Tkinter.Label(self,text='Audio Source')


		# String for compressor
		self.entryCompressorPara = Tkinter.StringVar()
		self.entryCompressor = Tkinter.Entry(self,textvariable=self.entryCompressorPara)
		self.compand_profile = Tkinter.StringVar()
		
		if 'CompressorProfile' in self.data:
			self.askopenfile(self.data['CompressorProfile'])
		if 'CompressorData' in self.data:
			self.entryCompressorPara.set(self.data['CompressorData'])
		else:		
			self.entryCompressorPara.set('0.3,1 -90,-90,-50,-50,-40,-20,0,0 -5 0 0.2')
			self.compand_profile.set('Default Profile')
				
		label_Compr = Tkinter.Label(self,text='Audio Compressor Profile')#


		# Button for Audio Preview
		self.buttonVarAudioPreview = Tkinter.StringVar()
		buttonAudioPreview = Tkinter.Button(self,textvariable=self.buttonVarAudioPreview, command=self.AudioPreviewButtonClick)
		self.buttonVarAudioPreview.set(u"Play")

		# Button for Audio Original
		self.buttonVarAudioOriginal = Tkinter.StringVar()
		buttonAudioOriginal = Tkinter.Button(self,textvariable=self.buttonVarAudioOriginal, command=self.AudioOriginalButtonClick)
		self.buttonVarAudioOriginal.set(u"Play")
		
		
		# Button to apply compressor and save all into a new video file
		self.buttonVarApplyCompressor = Tkinter.StringVar()
		buttonApplyCompressor= Tkinter.Button(self,textvariable=self.buttonVarApplyCompressor, command = self.ApplyCompressorButtonClick)
		self.buttonVarApplyCompressor.set(u"Apply Compressor to Audio")
		
		# Save Load compressor profiles
		save_compand = Tkinter.Button(self, text='Save As', command=self.asksaveasfile)
		delete_compand = Tkinter.Button(self, text='Delete', command=self.delete_compressor_settings)
		open_compand = Tkinter.Button(self, textvariable=self.compand_profile, command=self.askopenfile)
		self.file_opt_compand = options_compand = {}
		options_compand['defaultextension'] = '.json'
		options_compand['filetypes'] = [ ('Compressor Profiles', '.json')]
		options_compand['initialdir'] =  cc_config + 'compressor_profiles/'
		options_compand['initialfile'] = ''
		options_compand['parent'] = self
		options_compand['title'] = 'Choose Compressor Profile'


		# Save Load personal profiles
		self.settings_variable=Tkinter.StringVar()
		save_personal = Tkinter.Button(self, text='Save As', command=self.save_personal_settings)
		delete_personal = Tkinter.Button(self, text='Delete', command=self.delete_personal_settings)
		open_personal = Tkinter.Button(self, textvariable=self.settings_variable, command=self.load_personal_settings)
		
		self.file_opt_personal_settings = options_personal = {}
		options_personal['defaultextension'] = '.json'
		options_personal['filetypes'] = [ ('Settings Profiles', '.json')]
		options_personal['initialdir'] =  cc_config + 'cc_profiles'
		options_personal['initialfile'] = ''
		options_personal['parent'] = self
		options_personal['title'] = 'Choose CaptureControl Profile'
		
		label_personal_profile = Tkinter.Label(self,text='CaptureControl Profile')
		
		if 'cc_profile' in self.data:
			self.settings_variable.set(self.short_url(self.data['cc_profile']))
		else:
			self.settings_variable.set('No Profile')

		# Pavu Control
		self.pavucontrol_var=Tkinter.StringVar()
		open_pavucontrol = Tkinter.Button(self, textvariable=self.pavucontrol_var, command=self.start_pavucontrol)
		self.pavucontrol_var.set(u'Start PavuControl')



# ------------------------------------------ Layout ---------------------------------------------------------------------
		self.grid_columnconfigure(1,weight=1)

		
		
		# CC Profile
		Tkinter.Label(self,text='Profile', font = 'bold').grid(column=0,row=0,sticky='W')
		label_personal_profile.grid(column=0,row=1, sticky = 'W')
		save_personal.grid(column=2,row=1,sticky='E')
		delete_personal.grid(column=3,row=1,sticky='W')
		open_personal.grid(column=1,row=1,sticky='EW')
		buttonRefresh.grid(column=4,row=1,sticky='EW')
		Tkinter.Label(self,text='', font = 'bold').grid(column=0,row=2,sticky='W')

		# MyPaint
		m=2
		Tkinter.Label(self,text='MyPaint', font = 'bold').grid(column=0,row=m+1,sticky='W')
		buttonMyPaint.grid(column=4,row=m+2,sticky='EW')
		Tkinter.Label(self,text='', font = 'bold').grid(column=0,row=m+3,sticky='W')

		# SSR
		s = m+3
		Tkinter.Label(self,text='SimpleScreenRecorder', font = 'bold').grid(column=0,row=s+1,sticky='W')
		buttonMuteSSR.grid(column=3,row=s+4,sticky='EW')
		buttonSSR.grid(column=4,row=s+2,sticky='EW')

		self.options_input_profile.grid(column = 1, columnspan = 2,row=s+2, sticky='EW')
		label_input_profile.grid(column=0,row=s+2,sticky='W')
		self.options_output_profile.grid(column = 1, columnspan = 2,row=s+3,sticky ='EW')
		label_output_profile.grid(column=0,row=s+3,sticky='W')
		self.entrySSR.grid(column = 1,columnspan=2,row=s+4,sticky='EW')		
		label_SSR_Filename.grid(column=0,row=s+4,sticky='W')

		Tkinter.Label(self,text='', font = 'bold').grid(column=0,row=s+5,sticky='W')

		# WebCam
		w=s+5
		Tkinter.Label(self,text='WebCam', font = 'bold').grid(column=0,row=w+1,sticky='W')
		buttonmplayer.grid(column=4,row=w+2,sticky='EW')

		label_webcam.grid(column=0,row=w+2,sticky='W')
		self.options_webcams.grid(column = 1,columnspan=2,row=w+2, sticky = 'EW')	
		label_WebCamFile.grid(column=0,row=w+3,sticky='W')
		self.entryWebCam.grid(column = 1, columnspan=2,row=w+3,sticky='EW')

		Tkinter.Label(self,text='', font = 'bold').grid(column=0,row=w+4,sticky='W')	

		# Compressor
		c=w+4
		Tkinter.Label(self,text='Audio Postprocessing', font = 'bold').grid(column=0,row=c+1,sticky='W')
		buttonAudioOriginal.grid(column=3,row=c+2, sticky = 'EW')

		label_audio_file.grid(column=0,row=c+2, sticky = 'W')
		self.entryAudio.grid(column=1, columnspan = 2 ,row=c+2,sticky='EW')

		label_Compr.grid(column=0,row=c+3, sticky = 'W')
		self.entryCompressor.grid(column=1, columnspan=2,row=c+4,sticky='EW')
		open_compand.grid(column=1, columnspan = 1,row=c+3,sticky='EW')
		save_compand.grid(column=2,row=c+3,sticky='E')
		delete_compand.grid(column=3,row=c+3,sticky='EW')

		buttonAudioPreview.grid(column=3,row=c+4, sticky='EW')
		buttonApplyCompressor.grid(column=4,row=c+4, sticky='EW')
		Tkinter.Label(self,text='Audio Compressor Parameters').grid(column=0,row=c+4,sticky='W')

		Tkinter.Label(self,text='', font = 'bold').grid(column=0,row=c+5,sticky='W')

		# Output Label
		o = c+5
		Tkinter.Label(self,text='Messages', font='bold').grid(column=0,row=o+1,sticky='W')
		label.grid(column=0,row=o+2,columnspan=3,sticky='EW')
		Tkinter.Label(self,text='', font = 'bold').grid(column=0,row=o+3,sticky='W')

		# KeyMon
		k = o+3
		Tkinter.Label(self,text='KeyMon', font = 'bold').grid(column=0,row=k+1,sticky='W')
		buttonKeyMon.grid(column=4,row=k+2,sticky='EW')
		label_keymon.grid(column=0,row=k+2,sticky='W')
		self.entry.grid(column = 1, columnspan=2,row=k+2,sticky='EW')
		Tkinter.Label(self,text='', font = 'bold').grid(column=0,row=k+3,sticky='W')


		# Pavucontrol
		p = k+3
		Tkinter.Label(self,text='PavuControl', font = 'bold').grid(column=0,row=p+1,sticky='W')
		open_pavucontrol.grid(column=4,row=p+2,sticky='EW')
		Tkinter.Label(self,text='Expert Sound Settings').grid(column=0,row=p+2,sticky='W')

# ---------------- Start webcam with key -----------

	def key(self,entry):
		#self.RefreshButtonClick()
		print 'pressed key'

# --------------- Check whitch processes have been closed ------------

	def repeater(self):                          
		#print self.running_processes
		
		for key in self.running_processes.keys():
			if self.running_processes[key].poll() != None:
				self.finish_process(key)				
		self.after(500, self.repeater) 	#check on every N miliseconds



	def finish_process(self,process_name):
		if process_name == 'KeyMon':
			self.buttonVarKeyMon.set(u'Start Key-Mon')
			del self.running_processes[process_name]
		if process_name == 'SSR':		
			self.stopped_SSR()		
			self.buttonVarMuteSSR.set(u'Mute SSR')
			del self.running_processes[process_name]
		if process_name == 'WebCam':
			self.buttonVarmplayer.set(u"Start Webcam")
			del self.running_processes[process_name]
		if process_name == 'MyPaint':
			self.buttonVarMyPaint.set(u"Start MyPaint")
			del self.running_processes[process_name]
		if process_name == 'AudioPreview':
			self.buttonVarAudioPreview.set(u"Play")
			del self.running_processes[process_name]	
		if process_name == 'AudioOriginal':
			self.buttonVarAudioOriginal.set(u"Play")
			del self.running_processes[process_name]	
		if process_name == 'PavuControl':
			self.pavucontrol_var.set(u'Start PavuControl')
			del self.running_processes[process_name]	


# ---------------- Fast Start -------------------

	def FastStartButtonClick(self):
		self.MyPaintButtonClick()
		time.sleep(1)
		self.mplayerButtonClick()
		time.sleep(1)
		self.SSRButtonClick()
		time.sleep(1)
		sp.call("./SSR_faststart.sh")		

#---------------- Start Key-Mon -----------------------------------------
	def KeyMonButtonClick(self):
		
		if self.buttonVarKeyMon.get() == u"Start Key-Mon" :
			self.labelVariable.set( " started Key Mon")
			self.buttonVarKeyMon.set(u"Stop Key-Mon")
			self.KeyMon = sp.Popen(['key-mon'] + self.entryKeyMonVar.get().split(), stdout=sp.PIPE)
			self.running_processes.update({'KeyMon':self.KeyMon})
			
		elif self.buttonVarKeyMon.get() == u"Stop Key-Mon" :
			self.labelVariable.set( " stopped Key-Mon")
			self.buttonVarKeyMon.set(u"Start Key-Mon")
			del self.running_processes['KeyMon']
			self.KeyMon.terminate()

# ------------------ Start Webcam ----------------------------	

	def asksaveasWebCam(self):
		
		
		self.filenameWebCam = tkFileDialog.asksaveasfilename(**self.file_opt_webcam)
		if self.filenameWebCam == '':
			return
		self.entryFileNameWebCam.set(self.filenameWebCam)
		
		self.file_opt_webcam['initialdir']= self.filenameWebCam[:self.filenameWebCam.rfind('/')+1]
		self.file_opt_webcam['initialfile']= self.filenameWebCam[self.filenameWebCam.rfind('/')+1:]
		#print self.filenameWebCam[self.filenameWebCam.rfind('/')+1:]

	def mplayerButtonClick(self):
		if self.buttonVarmplayer.get() == u"Start Webcam" :
			
			device_number = self.webcams[self.var_webcams.get()]
			write_guvcview_configures(self.var_webcams.get(), device_number, self.entryFileNameWebCam.get())
	
		 	self.labelVariable.set( " started Webcam")
			self.buttonVarmplayer.set(u"Stop Webcam")
			print self.webcams[self.var_webcams.get()]
			
			config_file_path = "--profile=/"+home+"/.config/guvcview/GUVCVideo_default_final.gpfl"
			self.mplayer=sp.Popen(['guvcview','-d',device_number,config_file_path], stdout=sp.PIPE)	
			self.running_processes.update({'WebCam':self.mplayer})
				

		elif self.buttonVarmplayer.get() == u"Stop Webcam" :
			self.labelVariable.set( " stopped Webcam")
			self.buttonVarmplayer.set(u"Start Webcam")
			
			self.mplayer.terminate()
			time.sleep(1)
	
			if self.mplayer.poll() == None:
				self.mplayer.kill()
				print 'mplayer killed'
#     Refresh available WebCams
	def RefreshButtonClick(self):
		store = self.var_webcams.get()
		self.refresh_ssr_profiles()
		self.options_webcams['menu'].delete(0,'end')
		self.webcams = choose_webcam()
		if store in self.webcams:
			self.var_webcams.set(store)
		else:
			self.var_webcams.set(self.webcams.iterkeys().next())
		for camera in self.webcams:
			self.options_webcams['menu'].add_command(label=camera,command=lambda camera=camera: self.var_webcams.set(camera))
	
	def refresh_ssr_profiles(self):
		store = self.var_input_profile.get()
		self.options_input_profile['menu'].delete(0,'end')
		self.SSRProfiles()
		if store in self.input_profiles:
			self.var_input_profile.set(store)
		else:
			self.var_input_profile.set(self.input_profiles.interkeys().next())
		for profile in self.input_profiles:
			self.options_input_profile['menu'].add_command(label = profile, command=lambda profile=profile: self.var_input_profile.set(profile))

		store2 = self.var_output_profile.get()
		self.options_output_profile['menu'].delete(0,'end')
		if store2 in self.output_profiles:
			self.var_output_profile.set(store2)
		else:
			self.var_output_profile.set(self.output_profiles.interkeys().next())
		for profile in self.output_profiles:
			self.options_output_profile['menu'].add_command(label = profile, command=lambda profile=profile: self.var_output_profile.set(profile))


#-------------- Start Stop SimpleScreenRecorder --------------------------------
	def asksaveasSSR(self):
		
		
		self.filenameSSR = tkFileDialog.asksaveasfilename(**self.file_opt_SSR)
		if self.filenameSSR == '':
			return
		self.entryFileNameVar.set(self.filenameSSR)
		self.compare_suffix()		
		
		self.filenameSSR = self.entryFileNameVar.get()
		self.file_opt_SSR['initialdir']= self.filenameSSR[:self.filenameSSR.rfind('/')+1]
		self.file_opt_SSR['initialfile']= self.filenameSSR[self.filenameSSR.rfind('/')+1:]
		#print self.filenameWebCam[self.filenameWebCam.rfind('/')+1:]
		
		self.entryFileNameAudio.set(self.filenameSSR)
		self.file_opt_audio['initialdir']= self.filenameSSR[:self.filenameSSR.rfind('/')+1]
		self.file_opt_audio['initialfile']= self.filenameSSR[self.filenameSSR.rfind('/')+1:]

	def SSRButtonClick(self):
		
		if self.buttonVarSSR.get() == u"Start SSR" :
			self.labelVariable.set( " started SSR")
			self.buttonVarSSR.set(u"SSR is running")			
			self.addDefaultFiles()
			self.write_ssr_configures()
			sp.call(['pacmd', 'set-default-source', 'alsa_input.usb-Creative_Technology_SB_X-Fi_Surround_5.1__blank_-00.analog-stereo'])
			self.SSR = sp.Popen('simplescreenrecorder', stdout=sp.PIPE)				
			self.running_processes.update({'SSR':self.SSR})	
		#filenameSSRfilenameSSR
	def stopped_SSR(self):			
		self.removeDefaultFiles()
		self.labelVariable.set( " stopped SSR")			
		self.buttonVarSSR.set(u"Start SSR")
		#self.refresh_ssr_profiles()

	def MuteSSRButtonClick(self):
		
		if self.buttonVarMuteSSR.get()==u'Mute SSR':
			mute("SimpleScreenRecorder")
			self.buttonVarMuteSSR.set(u'UnMute SSR')
			tkMessageBox.showinfo('CaptureControl','No Sound is recorded at the moment. \nTo record again, close this window.',icon='warning')
			

			unmute("SimpleScreenRecorder")
			self.buttonVarMuteSSR.set(u'Mute SSR')
			

#----------------- Start MyPaint ------------------------------------------
	def MyPaintButtonClick(self):
		
		if self.buttonVarMyPaint.get() == u"Start MyPaint" :  
			sp.call("./tablet_button_config.sh")    # Set the Keys of the tablet
			shutil.copyfile(cc_config + 'mypaint/settings.json', home+'/.mypaint/settings.json')
			shutil.copyfile(cc_config + 'mypaint/accelmap.conf', home+'/.mypaint/accelmap.conf')
			src_brushes = os.listdir( cc_config + 'mypaint/brushes/')
			for brush in src_brushes:
				full_brush_path = os.path.join(cc_config + 'mypaint/brushes/',brush)
				if (os.path.isfile(full_brush_path)):			
					shutil.copyfile(full_brush_path, home+'/.mypaint/brushes/'+brush)
			self.labelVariable.set( " started MyPaint")
			self.buttonVarMyPaint.set(u"MyPaint is running")
			self.MyPaint = sp.Popen('/usr/bin/mypaint', stdout=sp.PIPE)
			self.running_processes.update({'MyPaint':self.MyPaint})	
			
						
		#elif self.buttonVarMyPaint.get() == u"Stop MyPaint" :
		#	self.labelVariable.set( " stopped MyPaint")
			#self.record.terminate()
		#	self.buttonVarMyPaint.set(u"Start MyPaint")
		#
		#	self.MyPaint.terminate()
	
#------------- Search all the available profiles-------------
	def SSRProfiles(self):
		ssrinput_profiles = glob.glob(home+'/.ssr/input-profiles/*.conf')
		ssroutput_profiles = glob.glob(home+'/.ssr/output-profiles/*.conf')
		self.ccinput_profiles = glob.glob(SSR_config + 'input-profiles/*.conf')
		self.ccoutput_profiles = glob.glob(SSR_config + 'output-profiles/*.conf')
		
		input_profiles = self.ccinput_profiles+ssrinput_profiles
		input_short = [self.short_url(k) for k in input_profiles]
		output_profiles = self.ccoutput_profiles+ssroutput_profiles
		output_short = [self.short_url(k) for k in output_profiles]
		self.input_profiles = dict([(k,v) for k,v in zip(input_short, input_profiles)])  # create dict's
		self.output_profiles = dict([(k,v) for k,v in zip(output_short, output_profiles)])
	

# ---------------- Add / Remove Default Profiles from .ssr/ folder ------------------
	def addDefaultFiles(self):
		for files in glob.glob(SSR_config + 'input-profiles/sys_*.conf'):
			shutil.copyfile(files, home+'/.ssr/input-profiles/'+self.short_url(files)+'.conf')
		for files in glob.glob(SSR_config + 'output-profiles/sys_*.conf'):
			shutil.copyfile(files, home+'/.ssr/output-profiles/'+self.short_url(files)+'.conf')	

	def removeDefaultFiles(self):
		for files in glob.glob(home+'/.ssr/input-profiles/sys_*.conf'):
			os.remove(files)
		for files in glob.glob(home+'/.ssr/output-profiles/sys_*.conf'):
			os.remove(files)

	
		
#------------ Write the ssr settings.conf  ---------------------------------
	def write_ssr_configures(self):
		input_profile_name=self.var_input_profile.get() 
		output_profile_name=self.var_output_profile.get() 

		with open( self.input_profiles[self.var_input_profile.get()],'r') as input_file:
			input_data=input_file.read()
		with open( self.output_profiles[self.var_output_profile.get()],'r') as output_file:
			output_data=output_file.read()
		with open( SSR_config + 'record.conf','r') as record_file:
			record_data=record_file.read()
		#container=output_data[output_data.find('container=')+10:output_data.find('container=')+13]
		#output_data.replace('container='+container,'container='+self.entryFileNameVar.get()[-3:])
		#print 'container='+container
		#print 'container='+self.entryFileNameVar.get()[-3:]
		self.timestamp = time.strftime("_%Y_%m_%d_%H_%M")
		self.video_file= self.entryFileNameVar.get()[:-4]+self.timestamp + self.entryFileNameVar.get()[-4:]

		self.entryFileNameAudio.set(self.video_file)

		header_input = '[global]\nnvidia_disable_flipping=ask\n\n[input]\nprofile='+input_profile_name+'\n'
		header_output = '\n[output]\nprofile='+output_profile_name+'\nfile='+self.video_file
		

		with open(home+'/.ssr/settings.conf','w') as config:
			config.write(header_input+input_data[8:]+header_output+output_data[output_data.find('separate_files')-1:]+record_data)
		
	def short_url(self,url):
		return url[url.rfind('/')+1:url.rfind('.')]


	def compare_suffix(self,*args):
		print 'comparing'
		print args
		with open(self.output_profiles[self.var_output_profile.get()],'r') as output_file:
			output_data=output_file.read()
		container=output_data[output_data.find('container=')+10:output_data.find('container=')+13]
		
		if container != self.entryFileNameVar.get()[-3:]:
			print 'not the same'
			self.entryFileNameVar.set(self.entryFileNameVar.get()+'.'+container)
		else :			
			print 'the same'
		self.filenameSSR = self.entryFileNameVar.get()
		self.file_opt_SSR['initialdir']= self.filenameSSR[:self.filenameSSR.rfind('/')+1]
		self.file_opt_SSR['initialfile']= self.filenameSSR[self.filenameSSR.rfind('/')+1:]

	def compare_suffix_2(self,*args):
		print 'comparing'
		print args
		with open(self.output_profiles[self.var_output_profile.get()],'r') as output_file:
			output_data=output_file.read()
		container=output_data[output_data.find('container=')+10:output_data.find('container=')+13]
		
		if container != self.entryFileNameVar.get()[-3:]:
			print 'not the same'
			self.entryFileNameVar.set(self.entryFileNameVar.get()[:-3]+container)
		else :			
			print 'the same'
		self.filenameSSR = self.entryFileNameVar.get()
		self.entryFileNameAudio.set(self.filenameSSR)
		self.file_opt_audio['initialdir']= self.filenameSSR[:self.filenameSSR.rfind('/')+1]
		self.file_opt_audio['initialfile']= self.filenameSSR[self.filenameSSR.rfind('/')+1:]
		self.file_opt_SSR['initialdir']= self.filenameSSR[:self.filenameSSR.rfind('/')+1]
		self.file_opt_SSR['initialfile']= self.filenameSSR[self.filenameSSR.rfind('/')+1:]

# ------------------ To apply compressor on the audio stream --------------------------------
	def asksaveasfile(self):
		filenameCompand = tkFileDialog.asksaveasfilename(**self.file_opt_compand)		
		while 'sys_' in filenameCompand:
			tkMessageBox.showinfo('CaptureControl','This is a default file and cannot be edited. Please choose a filename without the string "sys_"')
			filenameCompand = tkFileDialog.asksaveasfilename(**self.file_opt_compand)

		if filenameCompand == '':
			return
		with open(filenameCompand,'w') as compand_profile:		
			#json.dump(filenameCompand,compand_profile)
			json.dump(self.entryCompressorPara.get(),compand_profile)
			self.compand_profile.set(filenameCompand[filenameCompand.rfind('/')+1:-5])

	def delete_compressor_settings(self):
		if 'sys_' in self.compand_profile.get():
			tkMessageBox.showinfo('CaptureControl','This is a system profile, you cannot delete it :-)')
			return			
		answer = tkMessageBox.askyesno('CaptureControl','Are you sure you that want to delete this Compressor profile?')
		print answer		
		if answer == True:
			os.remove( cc_config+'compressor_profiles/' + self.compand_profile.get() + '.json' )
			self.compand_profile.set('(None)')
			self.entryCompressorPara.set('')

	def askopenfile(self,filenameCompand_load=None):
		if filenameCompand_load == None:
			filenameCompand = tkFileDialog.askopenfilename(**self.file_opt_compand)
			if filenameCompand == '':
				return
		elif os.path.isfile(filenameCompand_load)==True:
			filenameCompand = filenameCompand_load
		else:
			return
		with open(filenameCompand,'r') as compand_profile:		
			self.compand_profile.set(filenameCompand[filenameCompand.rfind('/')+1:-5])
			self.entryCompressorPara.set( json.load(compand_profile) )


	def askopenAudio(self):

		self.filenameAudio = tkFileDialog.askopenfilename(**self.file_opt_audio)
		if self.filenameAudio == '':
			return
		self.entryFileNameAudio.set(self.filenameAudio)
		#print self.entryFileNameAudio.get()
		self.file_opt_audio['initialdir']= self.filenameAudio[:self.filenameAudio.rfind('/')+1]
		self.file_opt_audio['initialfile']= self.filenameAudio[self.filenameAudio.rfind('/')+1:]

	def AudioPreviewButtonClick(self):
		if self.buttonVarAudioPreview.get() == u'Play':
			if os.path.isfile(self.entryFileNameAudio.get()[:-4]+'_audio.ogg') == True:
				self.play_audio_preview()	
			else:
				self.split_video()
				self.play_audio_preview()

			self.buttonVarAudioPreview.set(u'Stop')
		else :
			self.play_preview.terminate()
			self.buttonVarAudioPreview.set(u'Play')

	def AudioOriginalButtonClick(self):
		if self.buttonVarAudioOriginal.get() == u'Play':
			if os.path.isfile(self.entryFileNameAudio.get()[:-4]+'_audio.ogg') == True:
				self.play_audio_original()	
			else:
				self.split_video()
				self.play_audio_original()

			self.buttonVarAudioOriginal.set(u'Stop')
		else :
			self.play_original.terminate()
			self.buttonVarAudioOriginal.set(u'Play')

	def split_video(self):
		self.labelVariable.set(u'Please Wait, Audio is being extracted')
		self.extract_audio()
		#time.sleep(1)
		self.labelVariable.set(u'Please Wait, Video is being extracted')
		self.extract_video()		
		self.labelVariable.set(u'Video and Audio extraction finished')
		#time.sleep(1)

	def extract_audio(self):
		audio_extractor = sp.call(['ffmpeg', '-i', self.entryFileNameAudio.get() ,'-c:a', 'copy', '-vn', self.entryFileNameAudio.get()[:-4]+'_audio.ogg'])#,stdout=sp.PIPE)
				
	def extract_video(self):
		video_extractor = sp.call(['ffmpeg', '-i', self.entryFileNameAudio.get() ,'-c:v', 'copy', '-an', self.entryFileNameAudio.get()[:-4]+'_video.mkv'])#,stdout=sp.PIPE)
		
		
	def play_audio_preview(self):
		self.play_preview = sp.Popen(['play',self.entryFileNameAudio.get()[:-4]+'_audio.ogg', 'compand']+ self.entryCompressorPara.get().split(), stdout=sp.PIPE)
		self.running_processes.update({'AudioPreview':self.play_preview})
				
	def play_audio_original(self):
		self.play_original = sp.Popen(['play',self.entryFileNameAudio.get()[:-4]+'_audio.ogg'], stdout=sp.PIPE)
		self.running_processes.update({'AudioOriginal':self.play_original})

	def ApplyCompressorButtonClick(self):
		if os.path.isfile(self.entryFileNameAudio.get()[:-4]+'_audio.ogg') != True:
			self.split_video()
		self.apply_compressor()
		self.combine_audio_video()


	def apply_compressor(self):
		#self.compand_parameters = [attack_decay, compression, gain, start, delay]
		self.labelVariable.set(u'Please Wait, Compressor Parameters are being applied')
		apply_comp = sp.call(['sox',self.entryFileNameAudio.get()[:-4]+'_audio.ogg',self.entryFileNameAudio.get()[:-4]+'_audio_final.ogg', 'compand']+ self.entryCompressorPara.get().split())#, stdout=sp.PIPE)
	
	def combine_audio_video(self):
		self.labelVariable.set(u'Please Wait, Final Video is generated')
		combine = sp.call(['ffmpeg', '-i', self.entryFileNameAudio.get()[:-4]+'_audio_final.ogg' ,'-i', self.entryFileNameAudio.get()[:-4]+'_video.mkv', '-c', 'copy', self.entryFileNameAudio.get()[:-4]+'_final.mkv'])#,stdout=sp.PIPE)
		self.labelVariable.set(u'Final Video file can be found as'+self.entryFileNameAudio.get())

	def start_pavucontrol(self):
		if self.pavucontrol_var.get() == u'Start PavuControl':
			self.pavucontrol = sp.Popen('pavucontrol',stdout=sp.PIPE)
			self.pavucontrol_var.set('Stop PavuControl')
			self.running_processes['PavuControl'] = self.pavucontrol
		else:
			self.pavucontrol.terminate() 
			self.pavucontrol_var.set('Start PavuControl')
			
# --------------------------- Save Load Personal Settings -----------------------------------------------
	def save_personal_settings(self):
		filename_personal_settings = tkFileDialog.asksaveasfilename(**self.file_opt_personal_settings)
		while 'sys_' in filename_personal_settings:
			tkMessageBox.showinfo('CaptureControl','This is a default file and cannot be edited. Please choose a filename without the string "sys_"')
			filename_personal_settings = tkFileDialog.asksaveasfilename(**self.file_opt_personal_settings)
		
		if filename_personal_settings == '':
			return
		
		with open(filename_personal_settings,'w') as settings_file:
			out_data = {'ssr_output' : self.var_output_profile.get(),
				'ssr_input' : self.var_input_profile.get(),	
				'filenameSSR':self.entryFileNameVar.get(),
				'webcam': self.var_webcams.get(),
				'filenameWebCam':self.entryFileNameWebCam.get(),
				'CompressorProfile' : cc_config+'compressor_profiles/'+self.compand_profile.get()+'.json',
				'key_mon' : self.entryKeyMonVar.get(),
				'CompressorData' : self.entryCompressorPara.get(),
				'cc_profile' : filename_personal_settings}
			json.dump(out_data, settings_file,sort_keys = True, indent = 4, ensure_ascii=False)
		self.settings_variable.set(self.short_url(filename_personal_settings))
	
	def delete_personal_settings(self):
		if 'sys_' in self.settings_variable.get():
			tkMessageBox.showinfo('CaptureControl','This is a system profile, you cannot delete it :-)')
			return		
		answer = tkMessageBox.askyesno('CaptureControl','Are you sure you that want to delete this profile?')
		print answer		
		if answer == True:
			os.remove( cc_config+'cc_profiles/' + self.settings_variable.get() + '.json' )
			self.settings_variable.set('(None)')

	def load_personal_settings(self):
		
		filename_personal_settings = tkFileDialog.askopenfilename(**self.file_opt_personal_settings)
		if filename_personal_settings == '':
			return
		
		self.settings_variable.set(self.short_url(filename_personal_settings))

		with open(filename_personal_settings,'r') as personal_settings:		
			self.personal_data = json.load(personal_settings )		
		
		#SSR Input
		if 'ssr_input' in self.personal_data and self.personal_data['ssr_input'] in self.input_profiles:
			self.var_input_profile.set(self.personal_data['ssr_input'])
		else:
			self.var_input_profile.set('Not Available')
		#SSR Output
		if 'ssr_output' in self.personal_data and self.personal_data['ssr_output'] in self.output_profiles:
			self.var_output_profile.set(self.personal_data['ssr_output'])
		else:
			self.var_output_profile.set('Not Available')
		#Webcam		
		if 'webcam' in self.personal_data and self.personal_data['webcam'] in self.webcams:
			self.var_webcams.set(self.personal_data['webcam'])
		else: 	
			self.var_webcams.set('Not Available')
		# File Name SSR		
		if 'filenameSSR' in self.personal_data:
			filenameSSR = self.personal_data['filenameSSR']
			self.entryFileNameVar.set(filenameSSR)
			self.file_opt_SSR['initialdir'] =  filenameSSR[:filenameSSR.rfind('/')+1]
			self.file_opt_SSR['initialfile'] = filenameSSR[filenameSSR.rfind('/')+1:]
		else: 	
			self.entryFileNameVar.set("Not Available")

		# File Name WebCam
		if 'filenameWebCam' in self.personal_data:
			filenameWebCam = self.personal_data['filenameWebCam']
			self.entryFileNameWebCam.set(filenameWebCam)
			self.file_opt_webcam['initialdir'] =  filenameWebCam[:filenameWebCam.rfind('/')+1]
			self.file_opt_webcam['initialfile'] = filenameWebCam[filenameWebCam.rfind('/')+1:]
		else: 	
			self.entryFileNameWebCam.set("Not Available")
		# Compressor Profile
		if 'CompressorProfile' in self.personal_data:
			self.askopenfile(self.personal_data['CompressorProfile'])
		else:		
			self.entryCompressorPara.set('')
			self.compand_profile.set('Not Available')
		if 'key_mon' in self.personal_data: 
			self.entryKeyMonVar.set(self.personal_data['key_mon'])
		else:
			self.entryKeyMonVar.set('')

# ------------------ save load settings, that everything is the same after restart --------------------

	def save_settings(self):
		with open(cc_config+'cc_settings.json','w') as settings_file:
			out_data = {'ssr_output' : self.var_output_profile.get(),
				'ssr_input' : self.var_input_profile.get(),	
				'filenameSSR':self.entryFileNameVar.get(),
				'webcam': self.var_webcams.get(),
				'filenameWebCam':self.entryFileNameWebCam.get(),
				'CompressorProfile' : cc_config+'compressor_profiles/'+self.compand_profile.get()+'.json',
				'CompressorData' : self.entryCompressorPara.get(),
				'key_mon' : self.entryKeyMonVar.get(),
				'cc_profile' : cc_config+'cc_profiles/'+self.settings_variable.get()+'.json'}
			json.dump(out_data, settings_file,sort_keys = True, indent = 4, ensure_ascii=False)
			


	def load_settings(self):
		if os.path.isfile(cc_config+'cc_settings.json') == True:
			with open(cc_config+'cc_settings.json','r') as settings_file:
				self.data = json.load(settings_file)
				
		else:
			self.data = []
		
		if 'cc_profile' in self.data and os.path.isfile(self.data['cc_profile']):  # start == 1 and
			with open(self.data['cc_profile'],'r') as personal_file:
				self.data = json.load(personal_file)
	
	def compare_profile(self):
		final_data = {'ssr_output' : self.var_output_profile.get(),
			'ssr_input' : self.var_input_profile.get(),	
			'filenameSSR':self.entryFileNameVar.get(),
			'webcam': self.var_webcams.get(),
			'filenameWebCam':self.entryFileNameWebCam.get(),
			'CompressorProfile' : cc_config+'compressor_profiles/'+self.compand_profile.get()+'.json',
			'cc_profile' : cc_config+'cc_profiles/'+self.settings_variable.get()+'.json',
			'key_mon' : self.entryKeyMonVar.get(),
			'CompressorData' : self.entryCompressorPara.get()}
		if os.path.isfile(final_data['cc_profile']):
			with open(final_data['cc_profile'],'r') as personal_settings:		
				profile_data = json.load(personal_settings ) 		
			if final_data == profile_data:
				self.destroy()
		else:
			if tkMessageBox.askokcancel('CaptureControl','There are unsaved changes, do you want to continue without saving?'):
				self.destroy()
def hello(event):
	print 'hello'

#------------------ start main loop =-------------------------		
if __name__ == "__main__":
	
	app=simpleapp_tk(None)
	app.title('CaptureControl')
	app.protocol("WM_DELETE_WINDOW",app.compare_profile)
	app.protocol("WM_HOTKEY",hello)
	app.mainloop()
	app.save_settings()

















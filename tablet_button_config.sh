#!/bin/bash
#
# $Id: crose 2015-02-16 17:13:14+01:00$
#
#

WACOM_MEDIUM="Wacom Intuos Pro M"
WACOM_LARGE="Wacom Intuos Pro L"
 
for TYPE in "$WACOM_MEDIUM" "$WACOM_LARGE" ; do

	# $ xsetwacom list
	# Wacom Intuos Pro L Pen stylus    	id: 11	type: STYLUS    
	# Wacom Intuos Pro L Pen eraser    	id: 12	type: ERASER    
	# Wacom Intuos Pro L Finger touch  	id: 16	type: TOUCH     
	# Wacom Intuos Pro L Finger pad    	id: 17	type: PAD      


	# To activate touch controls again, simply uncomment following line
	# xsetwacom set "$TYPE Finger touch" Gesture on
	xsetwacom set "$TYPE Finger touch" Gesture off


	# Defining the Buttons 1-4 on the pad. With this config the first button presses 1 on your keyboard.
	# Note that the first Button is "2" not "1", etc. 

	# Button 1 is the middle button of the wheel
	xsetwacom set "$TYPE Pen pad" Button 1 "key F11"
	xsetwacom set "$TYPE Pen pad" Button 2 "key 1"
	xsetwacom set "$TYPE Pen pad" Button 3 "key 2"
	xsetwacom set "$TYPE Pen pad" Button 8 "key 3"
	xsetwacom set "$TYPE Pen pad" Button 9 "key 4"

	#xsetwacom set "$TYPE Pen pad" Button 10 "key 5"
	#xsetwacom set "$TYPE Pen pad" Button 11 "key 6"
	#xsetwacom set "$TYPE Pen pad" Button 12 "key 7"
	#xsetwacom set "$TYPE Pen pad" Button 13 "key 8"

	# To make the brush bigger / smaller
	xsetwacom set "$TYPE Pen pad" Button 10 "key f"
	xsetwacom set "$TYPE Pen pad" Button 11 "key d"



	# to use two lower keys to move up and down the canvas
	xsetwacom set "$TYPE Pen pad" Button 12 "key down"
	xsetwacom set "$TYPE Pen pad" Button 13 "key up"
	#xsetwacom set "$TYPE Pen pad" AbsWheelUp "key alt super ="
	#xsetwacom set "$TYPE Pen pad" AbsWheelDown "key alt super -"
	# to zoom in out with the two last keys
	xsetwacom set "$TYPE Pen pad" AbsWheelUp "key ="
	xsetwacom set "$TYPE Pen pad" AbsWheelDown "key -"
done




#!/usr/bin/python
#
# $Id: crose 2015-02-16 17:13:14+01:00$
#
#

import subprocess as sp

def choose_webcam():
	videos = sp.Popen(['v4l2-ctl','--list-devices'],stdout=sp.PIPE)  # check all input devices
	out, err = videos.communicate()				 # make a string out of it
	return find_all(out)

		


def find_all(string):
	start_old=0
	available_cams = {}
	
	while True:
		
		start_new = string.find('/dev/video',start_old)  	# find if there is a device
		if start_new == -1: 					# if no more devices are available, return
			return available_cams				
		name = string[start_old:string.find('(',start_old)-1]	# The name of the device
		device = string[start_new:start_new+11]			# internal
		i=1
		while name in available_cams:				# to make sure that a key is not written twice
			name = name+'_'+str(i)
			i+=1		
		available_cams[name]=device				# append create a dict name:device
		start_old = start_new + 13				# go to next device


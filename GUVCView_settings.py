#!/usr/bin/python
#
# $Id: crose 2015-02-16 17:13:14+01:00$
#
#

import shutil

from os.path import expanduser
home = expanduser("~")

cc_config = home+'/.capturecontrol/'	
		
#------------ Write the settings for the webcam -----------------------
def write_guvcview_configures(camera,device,filename):
	if camera == 'BCC950 ConferenceCam':
		with open( cc_config + 'GUVCView/default_ConferenceCam','r') as f:
			config_data = f.read()
	elif camera == 'FaceTime HD Camera':
		with open( cc_config + 'GUVCView/default_FaceTimeCam','r') as f:
			config_data = f.read()
	else:
		with open( cc_config + 'GUVCView/default_Video','r') as f:
			config_data = f.read()
	config_data = config_data.replace("/dev/video1",device)
	config_data = config_data.replace("/data/temp/replace_video_path", filename)
	print filename
	
	with open( home+'/.config/guvcview/'+device[device.rfind('/')+1:],'w') as config:
		config.write(config_data)

	shutil.copyfile(cc_config + 'GUVCView/GUVCVideo_default_final.gpfl', home+'/.config/guvcview/GUVCVideo_default_final.gpfl')





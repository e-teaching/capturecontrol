#!/bin/bash
#
# $Id: crose 2015-02-16 17:13:14+01:00$
#
#

ssr_win=$(xdotool search --onlyvisible --name 'SimpleScreenRecorder')
xdotool windowactivate $ssr_win
xdotool mousemove --window $ssr_win 400 620  # Move Mouse to "Continue"
xdotool click --repeat 3 --delay 000 1 		# Click 3 times "Continue" (Start Screen, Input, Ouput, arriving at recording)
xdotool mousemove --window $ssr_win 400 460  # go to "preview button
xdotool click 1					# start preview


guvcview_win=$(xdotool search --onlyvisible --name 'GUVCViewer Controls')
xdotool windowactivate $guvcview_win
xdotool mousemove --window $guvcview_win 280 60  
sleep 0.05
xdotool click --delay 0 1			#Start recording the webcam. Delay before is to ensure that the window is active
xdotool windowactivate $ssr_win
xdotool mousemove --window $ssr_win 400 45
sleep 0.05
xdotool click --delay 0 1			# start recording the screen. Delay before is to ensure that the window is active


guvcview_win2=$(xdotool search --onlyvisible --name 'GUVCVideo')  # Size change of the Webcam stream and set it to the front
xdotool windowactivate $guvcview_win2
xdotool windowsize $guvcview_win2 360 240
wmctrl -r 'GUVCVideo' -b add,above
MyPaintWin=$(xdotool search --onlyvisible --name 'MyPaint')  # get mypaint to the front
xdotool windowactivate $MyPaintWin

